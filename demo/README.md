# Machine Provisioning Demo

1. Create two RHPDS systems: AAP and AWS open env
1. Copy `vars.example.yml` to `vars.yml` and `secrets.example.sh` to `secrets.sh`
1. Log in to AWS console and copy the only subnet's _Subnet ID_ into `vars.yml` `vpc_subnet_id`.
1. Start to launch an instance and copy the Red Hat _AMI ID_ into `vars.yml` `ami_id`.
1. Ensure `vars.yml` `aws_region` matches the region of the RHPDS open environment (check email).
1.
1. Fill out `secrets.sh`

```bash
$ source ~/venv/bin/ansible/activate
$ source secrets.sh
$ ansible-playbook playbook.yml
```

All users will be created with the value of `CONTROLLER_PASSWORD` from `secrets.sh`.
